# :alembic: Getting started with scala

This repository contains multiples tests with a documentation to understand some scala and algorithm principles

## :bookmark: Summary

In `src/test/scala` :

- `TestScala` contains multiples tests to demonstrate the syntax,
- `calculus/TestComplex` test complex object to show off function
- `generator/TestStringCombination` test to show off all possibles string permutation from a size span and a char Seq.

## :satellite: How to run tests

- __If you use Intellij Idea editor__: Get the contextual menu of folder `src/test/scala` and select `Run ScalaTests in scala`
- __If you use SBT from cli__: simply type `sbt test`

## :book: Documentation

You'll find some notes and documentation directly into the [wiki](https://github.com/benjaminBoboul/workbench-getting-started-with-scala/wiki) section of this repository

